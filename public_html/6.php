<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>
<body class="typography">
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">#Digibury</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </div><!--/.navbar-collapse -->
  </div>
</div>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <canvas id="canvas" width="260" height="86"></canvas>
      </div>
      <div class="col-lg-9">
        <p>Digibury is the meet-up event for digital types in Kent!</p>

        <p>We meet up on Wednesday 2.0 (the second Wednesday in the month - oh hai!) in Canterbury for talks, demos, messing about with digital stuff and drinks.</p>

        <p>Around 50 of us show up every month and we have 2 or 3 speakers giving talks which are short, sweet and personal with plenty of time for questions and conversation.</p>
        <p><a class="btn btn-primary btn-lg btn-success">Sign up for the mailing list &raquo;</a></p>
      </div>
    </div>
  </div>
</div>

<div class="container">

  <div class="row">
    <div class="col-lg-4">
      <h2 class="wordart" title="Simon Bostock">Simon Bostock</h2>
      <p>Digibury is organised by Simon Bostock. He's on <a href="https://twitter.com/#!/i4_1" target="_blank">Twitter</a>, <a href="http://www.linkedin.com/in/bostocks" target="_blank">LinkedIn</a> or email ― simonb [at] deeson.co.uk.</p>

      <p>Simon works as a strategist/planner at Deeson Online (when he's not hounding the interesting people of Kent to share their geeky life stories and digital smarts at Digibury).</p>
      <p><a class="btn btn-default" href="#">Find out more &raquo;</a></p>
    </div>
    <div class="col-lg-4">
      <h2 class="wordart" title="Tim Deeson">Tim Deeson</h2>
      <p>Digibury was founded and is sponsored by Deeson Group MD and Drupal Don, Tim. The story of why Deeson founded Digibury is <a href="http://deeson-online.co.uk/blog/digital-canterbury-digibury" target="_blank">here on our blog</a>. Come and say hi to one of us if it's your first time.</p>

      <p>Tim's on <a href="https://twitter.com/#!/timdeeson" target="_blank">Twitter</a> and <a href="http://uk.linkedin.com/in/timdeeson" target="_blank">LinkedIn</a> too.</p>
      <p><a class="btn btn-default" href="#">Find out more &raquo;</a></p>
   </div>
    <div class="col-lg-4">
      <h2 class="wordart" title="Who? You!">Who? You!</h2>
      <p>Around 40 - 50 people come to #digibury every month from all over Kent. Some of us work in digital, but most people are basically mad keen on the web, tech and other creative stuff. Most of them follow this <a href="https://twitter.com/#!/digibury" title="Digibury on the Twitter">Twitter account</a>, or sign up on <a href="http://www.meetup.com/Digibury/" title="Digibury on MeetUp">MeetUp</a>.</p>

      <p>Have a look - do you know anybody?</p>
      <p><a class="btn btn-default" href="#">Find out more &raquo;</a></p>
    </div>
  </div>

  <hr>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>