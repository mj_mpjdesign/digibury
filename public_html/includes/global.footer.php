    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <small><a href="http://www.mpjdesign.ltd.uk" target="_blank" title="mpjdesign" id="credit">By mpjdesign</a>. We're not proud.</small>
          </div>

          <div class="col-lg-3 col-lg-offset-5">
            <?php $url = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)); ?>

            <?php if ((int)$url[1] === 1): ?>
            <a href="#" class="btn disabled">&laquo; Previous</a>
            <?php else: ?>
            <a href="/<?=(int)$url[1] - 1;?>" class="btn" id="prev">&laquo; Previous</a>
            <?php endif; ?>
            | <a href="/<?=(int)$url[1] + 1;?>" class="btn" id="next">Next &raquo;</a>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.0/js/bootstrap.min.js"></script>
  <script src="/resources/js/prism.js"></script>
  <script src="/resources/js/compiled.js"></script>
</body>
</html>