<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>
<body>
  <div class="container">

    <div class="starter-template">
      <h1>#Digibury: Back to the Future</h1>
      <p class="lead"><img src="/resources/images/digibury-back-to-the-future.jpg" alt="" width="100%" height="466"/></p>

      <p><a href="/1" class="btn btn-primary btn-lg btn-success">Let's roll &raquo;</a></p>
    </div>

  </div>

</body>
</html>