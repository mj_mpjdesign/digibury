<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Logo in HTML5 Canvas</h1>
        <canvas id="canvas" width="260" height="86"></canvas>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-coffeescript">
  c = document.getElementById "canvas"
  ctx = c.getContext "2d"
  grd = ctx.createLinearGradient 0, 0, 400, 0

  grd.addColorStop 0, "red"
  grd.addColorStop 1, "white"

  ctx.fillStyle = grd
  ctx.fillRect 0, 0, 800, 80

  ctx.font = "60px Comic Sans MS"
  ctx.strokeStyle = "blue"
  ctx.lineWidth = 3
  ctx.strokeText "Digibury", 10, 50
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>