<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Web fonts</h1>
        <div class="typography">
          <p><a class="btn btn-default" href="#">Find out more &raquo;</a></p>
        </div>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-css">
  @font-face {
    font-family: 'blackrose';
    src: url('/resources/fonts/BLACKR-webfont.eot');
    src: url('/resources/fonts/BLACKR-webfont.eot?#iefix') format('embedded-opentype'),
         url('/resources/fonts/BLACKR-webfont.woff') format('woff'),
         url('/resources/fonts/BLACKR-webfont.ttf') format('truetype'),
         url('/resources/fonts/BLACKR-webfont.svg#black_roseregular') format('svg');
    font-weight: normal;
    font-style: normal;
  }

  .typography p .btn {
    font-family: 'blackrose';
  }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>