<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Valid HTML!</h1>
        <p>Link: <a href="http://digibury.mpjdesign.com">http://digibury.mpjdesign.com</a></p>
        <p>Git: <a href="https://bitbucket.org/mj_mpjdesign/digibury" target="_blank">https://bitbucket.org/mj_mpjdesign/digibury</a></p>
      </div>

      <div class="col-lg-8">
        <img src="/resources/images/validated.png">
      </div>
    </div>

  </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <small><a href="http://www.mpjdesign.ltd.uk" target="_blank" title="mpjdesign" id="credit">By mpjdesign</a>. We're not proud.</small>
          </div>

          <div class="col-lg-3 col-lg-offset-5">

          <a href="/17" class="btn">&laquo; Previous</a>
          | <a href="#" class="btn disabled">Next &raquo;</a>
          </div>
        </div>
      </div>
    </footer>
</body>
</html>