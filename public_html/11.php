<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Long live <code>&lt;marquee></code></h1>
        <div class="marquee" style="max-width:640px"><p><span>Digibury is the meet-up event for digital types in Kent!</span><span>Digibury is the meet-up event for digital types in Kent!</span></p></div>

        <ul>
          <li>Have to enter the text twice (so that we don't get a long line of blank space)</li>
          <li>Set a specific size and hide everything that falls outside that box</li>
          <li>CSS3 keyframe animations</li>
        </ul>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-markup">
&lt;div class="marquee">&lt;p>&lt;span>Digibury is the meet-up event for digital types in Kent!&lt;/span>&lt;span>Digibury is the meet-up event for digital types in Kent!&lt;/span>&lt;/p>&lt;/div></code>
        </pre>

        <pre>
          <code class="language-css">
  .marquee {
    height: 30px;

    overflow: hidden;
    position: relative;
  }

  .marquee p {
    display: block;
    width: 200%;
    height: 30px;

    position: absolute;
    overflow: hidden;

    -webkit-animation: marquee 4s linear infinite;
    animation: marquee 4s linear infinite;
  }

  .marquee span {
    float: left;
    width: 50%;
  }

  @-webkit-keyframes marquee {
    0% { left: 0; }
    100% { left: -100%; }
  }

  @keyframes marquee {
    0% { left: 0; }
    100% { left: -100%; }
  }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>