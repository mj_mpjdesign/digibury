<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>&hellip;and long live <code>&lt;blink></code>, too</h1>
        <h2 class="blink">Yippee</h2>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-css">
  .blink {
    -webkit-animation: blink .75s linear infinite;
    animation: blink .75s linear infinite;
  }

  @-webkit-keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
  }

  @keyframes blink {
    0% { opacity: 1; }
    50% { opacity: 1; }
    50.01% { opacity: 0; }
    100% { opacity: 0; }
  }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>