<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Bevel</h1>
        <div class="typography bevel">
          <p><a class="btn btn-default" href="#">Find out more &raquo;</a></p>
        </div>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-css">
           .btn {
             box-shadow:
               inset 4px 4px 4px 1px rgba(255,255,255,0.5),
               inset -4px -4px 4px 1px rgba(0,0,0,0.6);
             }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>