<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Wordart effect</h1>
        <h2 class="wordart" title="Yes, seriously.">Yes, seriously.</h2>

        <ul>
          <li>Hide the text displayed (make it transparent)</li>
          <li>Use the title attribute to set what will be shown</li>
          <li>:before
            <ul>
              <li>Show the title attribute of the text</li>
              <li>Give the whole block a gradient</li>
              <li>Use the text as a transparent mask (<code>-webkit-background-clip</code>)</li>
            </ul>
          </li>
          <li>:after
            <ul>
              <li>Show the title attribute of the text</li>
              <li>CSS transforms to skew it into a shadow shape</li>
              <li>Opacity to change the colour</li>
            </ul>
          </li>
        </ul>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-css">
  .wordart {
    -webkit-text-fill-color: transparent;

    position: relative;
    display: inline-block;

    font: bold 48px "Arial Narrow", sans-serif;
  }

  .wordart:before {
    content: attr(title);

    position: absolute;
    left: 0;
    top: 0;
    z-index: 2;

    background: -webkit-linear-gradient(left, rgb(176,9,151) 0%, rgb(227,33,107) 10%, rgb(249,96,30) 25%, rgb(255,180,18) 40%, rgb(255,216,45) 50%, rgb(185,221,23) 61%, rgb(65,160,60) 75%, rgb(19,54,180) 90%, rgb(116,27,157) 100%);

    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }

  .wordart:after {
    content: attr(title);

    position: absolute;
    z-index: 199;
    left: -6px;
    bottom: -10px;

    z-index: 1;

    -webkit-text-fill-color: #000;
    -webkit-transform: skew(40deg, 0deg) scaleY(.5);

    opacity: .3;
  }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>