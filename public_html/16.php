<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.header.php'; ?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4">
        <h1>Dizzy?</h1>
        <h2 class="spin">More yippee</h2>
      </div>

      <div class="col-lg-8">
        <pre>
          <code class="language-css">
  .spin {
    display: inline-block;
    position: relative;

    -webkit-animation: nausea .25s linear infinite;
    animation: nausea .25s linear infinite;
  }

  @-webkit-keyframes nausea {
    0% { -webkit-transform: rotate(0deg); }
    50% { -webkit-transform: scale(1.5) rotate(180deg); }
    100% { -webkit-transform: scale(.8) rotate(360deg); }
  }

  @keyframes nausea {
    0% { transform: rotate(0deg); }
    50% { transform: scale(1.5) rotate(180deg); }
    100% { transform: scale(.8) rotate(360deg); }
  }
          </code>
        </pre>
      </div>
    </div>

<?php include $_SERVER['DOCUMENT_ROOT'].'/includes/global.footer.php'; ?>