c = document.getElementById "canvas"
ctx = c.getContext "2d"
grd = ctx.createLinearGradient 0, 0, 400, 0

grd.addColorStop 0, "red"
grd.addColorStop 1, "white"

ctx.fillStyle = grd
ctx.fillRect 0, 0, 800, 80

ctx.font = "60px Comic Sans MS"
ctx.strokeStyle = "blue"
ctx.lineWidth = 3
ctx.strokeText "Digibury", 10, 50